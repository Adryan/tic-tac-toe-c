#include<stdio.h>
#include<stdbool.h>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <conio.h>
#include <windows.h>
#define Empty NULL
#define Head(L) (L).Head
#define Player(P) (P)->player
#define IsBot(P) (P)->isbot
#define Next(P) (P)->next
#define Mark(P) (P)->mark

typedef char info[20];
typedef struct players *address;
struct players {
    info player;
    char mark;
    bool isbot;
	address next;
    };
typedef struct {
     address Head;	
} List;
typedef struct {
     int row;
     int col;
     int isi;
} SmallBoard;

void CreateList(List *L);
void EntryPlayer(List *L, info Player );
void EntryBot(List *L);
void PrintAllPlayers(List L);
void PrintPlayer(List L, int position);
char GetPlayerMark(address Position);
void SetPlayerMark(List *L, int position, char mark);
address AllocationPlayer(info Data);
address AllocationBot(info Data);
address PointFirstTurn(List L, int position);
void PrintPlayerNameTurn(address Position);
void PointToNextTurn(address *Position);
bool CheckPlayerIsBot(List L, int position);
int CountPlayer(List L);
void SetColor(int ForgC);
void gotoxy(int x, int y); 
void MaximizeWindows(void);
void setcolor(int warna);
void CreateBoard(int size);
