// BACKUP CODINGAN SELASA 10 MEI 2016 JAM 10.12
#include "tictactoe.h"
#define panjang 600

// Bismillahirahmanirahim
void First_Cursor_Position();
void x_symbol(int x,int y);
void o_symbol(int x,int y);
void x_symbol2(int x,int y);
void o_symbol2(int x,int y);
void cursor(int a); 
int cursormenu(int,int,int);
void next_match();
int play(int giliranpertama);
void tampilan_awal();
void menu_utama();
void player();
void winner();
void oneplayer();
void multiplayer();
void exit();
void rule();
void aboutus();
void BoardGame();
void BoardInfo();
int KocokDadu();
void ClearContentBoard();
void MasukanPemain(int jumlahplayer);
int GiliranKocok(int jumlahplayer);
int PreparingGame(int jumlahplayer);
void Informasi_Giliran(int giliranpertama);
void cursor(int a, int turn);


bool firstgame = true;
int konten[6][6];
int x_kor = 0,y_kor = 0;								//variabel global untuk menentukan posisi kursor saat pemilihan dan penyesuaian dengan board3x3
int board3x3[4][4];
SmallBoard sboard[9];

int iswinner;
List pemain;
int menang_board2 = 0;
int Xman, Yman;
char thewinner;
address penunjuk_giliran;
int counter = 1;
int zplayer = 0;

	void fillSmallBoard(){
		sboard[0].col = 1;		sboard[0].row = 1;		sboard[1].col = 2;		sboard[1].row = 1;
		sboard[2].col = 3;		sboard[2].row = 1;		sboard[3].col = 1;		sboard[3].row = 2;
		sboard[4].col = 2;		sboard[4].row = 2;		sboard[5].col = 3;		sboard[5].row = 2;
		sboard[6].col = 1;		sboard[6].row = 3;		sboard[7].col = 2;		sboard[7].row = 3;
		sboard[8].col = 3;		sboard[8].row = 3;
		for(int i=0; i<9; i++){
			sboard[i].isi = 0;
		}	
	}
	int check_array()						//mengecek array yang kosong saat pemilihan tirat "board3x3"
	{
		int choose;
		if(board3x3[y_kor][x_kor] == ' ')
		{
			choose = true;
		}
		else
		
		{
			choose= false;
		}
		return(choose);
	}
	void PrintBoard3x3Content(){
			for(int i=0;i<3;i++)
			{
				for(int j=0;j<3;j++)
				{
					if(board3x3[j+1][i+1] == 1){
						x_symbol(14+9*j,15+6*i);			//Penampilan simbol Y jika array board3x3 bernilai 0, dengan koordinat yang disesuaikan
					}else if(board3x3[j+1][i+1] == 2){
						o_symbol(14+9*j,15+6*i);			//Penampilan simbol Y jika array board3x3 bernilai 0, dengan koordinat yang disesuaikan
					}
				}
			}
			for(int g=0; g<9; g++){
				printf("Baris %d Kolom %d --->> %d\n",sboard[g].row, sboard[g].col, sboard[g].isi);
			}
	}
	void MasukanPemain(int jumlahplayer){
		int i;
		char nama[20];
		CreateList(&pemain);
		for(i=1;i<=jumlahplayer;i++){
			gotoxy(71,30+i); printf("Entry Player %d Name : ", i);
			scanf("%s",&nama);
			EntryPlayer(&pemain, nama);
		}
		for(i=1; i<=jumlahplayer; i++){
			gotoxy(71,30+i);printf("							");
		}
	}
	int PreparingGame(int jumlahplayer){
			int giliranpertama;
			char mark;
			MasukanPemain(jumlahplayer); // memasukan pemain sesuai jumlahnya
			if(jumlahplayer == 1){
				EntryBot(&pemain);
			}
			giliranpertama = GiliranKocok(jumlahplayer);
			penunjuk_giliran = PointFirstTurn(pemain, giliranpertama);
			x_symbol(71,30); o_symbol(86,30);
			SetColor(15);
			gotoxy(68,35); PrintPlayer(pemain, giliranpertama);printf("Set your mark (choose whether x or o) : "); mark = getch();
			SetPlayerMark(&pemain, giliranpertama, mark);
			gotoxy(68,36);PrintPlayer(pemain, giliranpertama); printf("Choose %c", mark);
			Sleep(2000);
			int k = 25;
			while(k <= 37){
				gotoxy(68,k);printf("												");
				k++;
			}
			return giliranpertama;
	}
	int minimax(int player) {
	    //How is the position like for player (their turn) on board?
//	    int winner = win(board);
//	    if(winner != 0) return winner*player;
	
	    int move = -1;
	    int score = -2;//Losing moves are preferred to no move
	    int i;
	    for(i = 0; i < 9; ++i) {//For all moves,
	        if(sboard[i].isi == 0) {//If legal,
	            sboard[i].isi = player;//Try the move
	            int thisScore = -minimax(player*-1);
	            if(thisScore > score) {
	                score = thisScore;
	                move = i;
	            }//Pick the one that's worst for the opponent
	            sboard[i].isi = 0;//Reset board after try
	        }
	    }
	    if(move == -1) return 0;
	    return score;	
	} 
	void computerMove(int turn) {
    	int move = -1;
	    int score = -2;
	    int i;
	    for(i = 0; i < 9; ++i) {
	        if(sboard[i].isi == 0) {
	            sboard[i].isi = 1;
	            int tempScore = -minimax(-1);
	            sboard[i].isi = 0;
	            if(tempScore > score)	{
	                score = tempScore;
	                move = i;
	            }
	        }
	    }
		Xman = sboard[move].row;
		Yman = sboard[move].col;

	    //returns a score based on minimax tree at a given node.
		printf("COMPUTER DO MOVE -> ROWS : %d    COLS : %d", Xman, Yman);
		
		Sleep(3200);		
		play(turn);
	}

	void match(int jumlahplayer, int PlayerTurn)
	{
		system("cls");
		int win,counter,available;
		int turn;
		int i,j;
		bool isbot;
		int x=14,y=15;
		int cekcursor = 1;
		int match = true;									//looping permainan selama bernilai true
		BoardInfo();
		CreateBoard(3);
		if(firstgame == true){	
			turn = PreparingGame(jumlahplayer);
		}else{
			turn = PlayerTurn;
		}	
		PrintAllPlayers(pemain);
		gotoxy(68,35);PrintPlayer(pemain, turn); printf("Now is your turn");
		if(iswinner == 1){ // Jika sudah mendapatkan pemenang
			system("cls");
			winner();
		}else{ // Jika belum mendapatkan pemenang
			PrintBoard3x3Content(); // print ulang board 3 x 3
			First_Cursor_Position(); // Pindahkan cursor ke kotak pertama
			ClearContentBoard(); // Menghapus board yang habis dimainkan
			isbot = CheckPlayerIsBot(pemain, turn); // Mengecek apakah bot atau bukan
			if(isbot){ // Jika bot lakukan yang dibawah
				computerMove(turn);
  		 	}else{ // Jika bukan lakukan ini
				cursor(cekcursor, turn);
				//kursor kembali ke tempat awal kursor saat memilih sel
			}
		}
	}
	void ClearContentBoard(){
		for(int i=1;i<=5;i++)
			{
				for(int j=1;j<=5;j++)
				{
					konten[i][j] = NULL;					
				}
			}
	}
int check(int isi, int x, int y, int board)
{
	int menang,n,k,b,i,j,b1,k1,b2,k2;
	int test1,test2, limit;
	int edge = 1;
	b = 1;
	k = 1;
	n = 1;
	if(board == 1){
		limit = 2;
	}else if(board == 2){
		limit = 3;
	}
	test1 = 0;
	while (n <= 4)
	{
		if (n == 1)
		{
			b = 1;
			k = 0;
		}
		if (n == 2)
		{
			b = 0;
			k = 1;
		}
		else if (n == 3)
		{
			b = 1;
			k = 1;
		}
		else if (n == 4)
		{
			b = 1;
			k = -1;
		}
		b1=x;
		k1=y;
		b2=x;
		k2=y;
		test2 = 0;		
		edge = 1;
        while (test2 == test1)
		{
		    if(board == 1){
		    	if (board3x3[b1][k1] == board3x3[b1+b][k1+k])
				{
					b1 = b1+b;
					k1 = k1+k;
					test1++;
				}
				else if (board3x3[b2][k2] == board3x3[b2-b][k2-k])
				{
					b2 = b2-b;
					k2 = k2-k;
					test1++;
				}
				test2++;	
		    }else{
				if (konten[b1][k1] == konten[b1+b][k1+k])
				{
					b1 = b1+b;
					k1 = k1+k;
					test1++;
				}
				else if (konten[b2][k2] == konten[b2-b][k2-k])
				{
					b2 = b2-b;
					k2 = k2-k;
					test1++;
				}
				test2++;	
		    }
		}
		n++;
	}
	if(test1 >= limit){
		return 1;
	}
	else{
		return 0;
	}
}

void cursor(int a, int turn){	
	// First Cursor //////////////////////////////////////////////////////////////////////////////////
	int X_Board=14, Y_Board=15;
	// Navigating the Board //////////////////////////////////////////////////////////////////////////		
	int countboard = 1;
	const char CPPKEYUP = 72;
	const char CPPKEYLEFT = 75;
	const char CPPKEYRIGHT = 77;
	const char CPPKEYDOWN = 80;
	const char ENTER = 13;
	int giliran = turn;
	int x_isi = 1;
	int y_isi = 1;
	char Hit = 0;
	Hit = kbhit(); 
	while(Hit != 27){
		Hit= getch();
		switch(Hit){
			case CPPKEYRIGHT:
				if(a == 1){
					if(X_Board + 9 <= 32){
						X_Board = X_Board + 9;
						x_isi += 1;
					}else{
						X_Board = 14;
						x_isi = 1;
					}
				}else{
					if(X_Board + 9 <= 50){
						X_Board = X_Board + 9;
						x_isi += 1;
					}else{
						X_Board = 14;
						x_isi = 1;
					}
				}
					gotoxy(X_Board, Y_Board);
				break;
			case CPPKEYLEFT:
				if(a == 1){
					if(X_Board - 9 >= 10){
						X_Board = X_Board - 9;
						x_isi -= 1;
					}else{
						X_Board = 32;
						x_isi = 3;
					}
										
				}else{
					if(X_Board - 9 >= 10){
					X_Board = X_Board - 9;
					x_isi -= 1;
					}else{
						X_Board = 50;
						x_isi = 3;
					}
				}

				gotoxy(X_Board, Y_Board);
				break;
			case CPPKEYUP:
				if(a == 1){
					if(Y_Board - 6 >= 10){
						Y_Board = Y_Board - 6;
						y_isi -= 1;
					}else{
						Y_Board = 27;
						y_isi = 3;				
					}
				}else{
					if(Y_Board - 6 >= 10){
						Y_Board = Y_Board - 6;
						y_isi -= 1;
					}else{
						Y_Board = 39;
						y_isi = 5;				
					}
				}

				gotoxy(X_Board, Y_Board);
				break;
			case CPPKEYDOWN:
				if(a == 1){
					if(Y_Board + 6 <= 32){
							Y_Board = Y_Board + 6;
							y_isi += 1;
						}else{
							Y_Board = 15;
							y_isi = 1;	
						}
				}else{
					if(Y_Board + 6 <= 40){
						Y_Board = Y_Board + 6;
						y_isi += 1;
					}else{
						Y_Board = 15;
						y_isi = 1;	
					}
				}
				gotoxy(X_Board, Y_Board);
				break;
			case ENTER:
				char simbol;
				info nama;
				int iswin;
				if(a == 1){
					if(board3x3[x_isi][y_isi] == NULL){
						Xman = x_isi;
						Yman = y_isi;
						play(turn);
					}
				}else{
					 if(konten[x_isi][y_isi] == NULL){//--------------------------------------------------------------------------------------------------------						 
						simbol = GetPlayerMark(penunjuk_giliran);						 
						if (zplayer == 2){
							 if(simbol == 'X'){
							 	if (counter == 1){
							 		x_symbol(X_Board, Y_Board);
		  					    	konten[x_isi][y_isi] = 1;
		  					    	counter = 2;
								}else{
									x_symbol2(X_Board, Y_Board);
		  					    	konten[x_isi][y_isi] = 1;
		  					    	counter = 1;
								}
							 }else if(simbol == 'O'){						 	
							 	if (counter != 1){
							 		o_symbol(X_Board, Y_Board);
	   						    	konten[x_isi][y_isi] = 2;
	   						    	counter = 2;
								}else{
									o_symbol2(X_Board, Y_Board);
	   						    	konten[x_isi][y_isi] = 2;
	   						    	counter = 1;
								}						 	
							 }
						}else{
							 if(simbol == 'X'){
							 		x_symbol(X_Board, Y_Board);
		  					    	konten[x_isi][y_isi] = 1;
							 }else if(simbol == 'O'){						 								 	
							 		o_symbol(X_Board, Y_Board);
	   						    	konten[x_isi][y_isi] = 2;	   						    	
							 }
						}
						 iswin = check(konten[x_isi][y_isi],x_isi,y_isi,2);
						 if(iswin == 1 || countboard>25){
						 	system("cls");
							if(countboard > 25){
								match(CountPlayer(pemain), 99);		
							}else{
								// Jika Menang mengisi board 3 x 3
								board3x3[Xman][Yman] = konten[x_isi][y_isi];
								int g=0;
								for(;;){
									if(sboard[g].row == Xman && sboard[g].col == Yman){
										break;	
									}
									g++;
								}
								if(penunjuk_giliran->isbot == true){
									sboard[g].isi = 1;	
								}else{
									sboard[g].isi = -1;
								}
								///////////////////
								//Jika menang mengecek apakah menang di board 3 x 3 juga
								iswinner = check(board3x3[x_isi][y_isi],Xman,Yman,1);
								if(iswinner == 1){
									thewinner = konten[x_isi][y_isi]; // Jika menang kita mendapatkan pemenang pertandingan
								}
								match(CountPlayer(pemain), konten[x_isi][y_isi]);		// Jika tidak kembali ke match
							}
						 }
						 PointToNextTurn(&penunjuk_giliran);
						 gotoxy(69,37);
					     setcolor(15);
						 PrintPlayerNameTurn(penunjuk_giliran);printf("now is your turn!!");
						 gotoxy(X_Board, Y_Board);
					}
				}
				break;
			case 'n':
				system("cls");
				int a = 1;
				while(a <= 3){
					int b = 1;
					while(b <= 3){
						printf("konten[%d][%d] = %d \t", a,b, board3x3[a][b]);
						b++;
					}
						printf("\n");
						a++;
				}
			break;
		}
	}
}

int play(int giliranpertama){
	system("cls");
	BoardInfo();
	PrintAllPlayers(pemain);
	int cek = 2;
	firstgame = false;
	CreateBoard(5);
	Informasi_Giliran(giliranpertama);
	First_Cursor_Position();
	cursor(cek, giliranpertama);	
}
void Informasi_Giliran(int giliranpertama){
	gotoxy(69,37);	PrintPlayer(pemain, giliranpertama); printf("your first turn");
}
// Modul Kursor
int cursormenu(int option,int x,int y)							
{
	int select=1;
	int input;
	gotoxy(x,y);printf("%c",175);
	do
	{ 
		input=getch();															//mengisi nilai input
		if(input==80)															//input key bergerak ke bawah
		{
			gotoxy(x,y);printf(" ");
			y++;select++;
			if(select>option)
			{
				y=y-option;
				select=1;
			}
			gotoxy(x,y);printf("%c",175);
		}
		if(input==72)															//input key bergerak ke atas 
		{
			gotoxy(x,y);printf(" ");
			y--;select--;
			if(select<1)
			{
				y=y+option;
				select=option;
			}
			gotoxy(x,y);printf("%c",175);			
		}															
	}
	while(input!=13);															//berakhir saat input berisi enter
	return select;																//modul memberikan output nilai select
}
// Modul Tampilan Awal
void tampilan_awal ()
{

	int i,j;
	char tamp_awal;
	char hline = 196;

	i=1;
	while (i<=30)
	{
	setcolor(12);gotoxy(i-1,15);   printf(" �������������  ���   �������������     �������������  ���������    �������������     �������������   ���������   ��������� \n");
	setcolor(12);gotoxy(i-1,16);   printf(" �������������  ���   �������������     �������������  ���������    �������������     �������������   ��     ��   ��������� \n");
	setcolor(12);gotoxy(59-i,17);  printf("      ���       ���  ���                     ���      ���     ���  ���                     ���       ��       ��  ��        \n");
	setcolor(12);gotoxy(i-1,18);   printf("      ���       ���  ���                     ���      ��       ��  ���                     ���       ��       ��  ��������� \n");
	setcolor(12);gotoxy(59-i,19);  printf("      ���       ���  ���                     ���      �����������  ���                     ���       ��       ��  ��        \n");
	setcolor(12);gotoxy(i-1,20);   printf("      ���       ���  ���                     ���      ��       ��  ���                     ���       ��       ��  ��        \n");
	setcolor(12);gotoxy(59-i,21);  printf("      ���       ���   �������������          ���      ��       ��   �������������          ���        ��     ��   ��������� \n");
	setcolor(12);gotoxy(59-i,22);  printf("      ���       ���   �������������          ���      ��       ��   �������������          ���        ���������   ��������� \n");	
		Sleep(10);
		i++;
	}		
	// Membuat garis atas dan bawah
	i=1;
	while(i<=170)
	{
		setcolor(7);gotoxy(i,10); printf("%c",hline);
					gotoxy(i,30); printf("%c",hline);
		i++;
		Sleep(5);
	}	
		// Menghapus Judul
	i=1;
	while (i<=65)
	{
		gotoxy(i-1,15);   printf("                                                                                                                        \n");
		gotoxy(i-1,16);   printf("                                                                                                                        \n");
		gotoxy(i-1,17);   printf("                                                                                                                        \n");
		gotoxy(i-1,18);   printf("                                                                                                                        \n");
		gotoxy(i-1,19);   printf("                                                                                                                        \n");
		gotoxy(i-1,20);   printf("                                                                                                                        \n");
		gotoxy(i-1,21);   printf("                                                                                                                        \n");
		gotoxy(i-1,22);   printf("                                                                                                                        \n");
	Sleep(5);
	i++;
	}		
	i=14;
	while (i<=21)
	{
		gotoxy(41,i); printf("                                                                              ");
		Sleep(30);
	i++;
	}
	//Hapus Garis
	i=1;
	while(i<=160)
	{
		gotoxy(i,10); 		printf(" ");
		gotoxy(160-i,30);	printf(" ");
		Sleep(10);
	i++;
	}
}
// Modul Menu Utama
void menu_utama ()
{
	setcolor(15);
	system("cls");
	char menu;
	gotoxy(50,15);printf("�������������������������������������������������������������������  \n");
	gotoxy(50,16);printf("��                                                               ��  \n");
	gotoxy(50,17);printf("��  ����       ����  ���������   ���        ���   ����      ���� ��  \n");
	gotoxy(50,18);printf("��  ��� ��   �� ���  ����        ��� ��     ���   ����      ���� ��  \n");
	gotoxy(50,19);printf("��  ���  �� ��  ���  ����        ���   ��   ���   ����      ���� ��  \n");
	gotoxy(50,20);printf("��  ���    ��   ���  ����        ���    ��  ���   ����      ���� ��  \n");
	gotoxy(50,21);printf("��  ���    ��   ���  ���������   ���     �� ���   ����      ���� ��  \n");
	gotoxy(50,22);printf("��  ���         ���  ���������   ���        ���   ����      ���� ��  \n");
	gotoxy(50,23);printf("��  ���         ���  ����        ���        ���   ����      ���� ��  \n");
	gotoxy(50,24);printf("��  ���         ���  ����        ���        ���   ����      ���� ��  \n");
	gotoxy(50,25);printf("��  ���         ���  ����        ���        ���   �������������� ��  \n");
	gotoxy(50,26);printf("��  ���         ���  ����������  ���        ���   �������������� ��  \n");
	gotoxy(50,27);printf("��                                                               ��  \n");
	gotoxy(50,28);printf("�������������������������������������������������������������������  \n");
	gotoxy(50,29),printf("\n");
	gotoxy(50,30);printf("                            P L A Y E R                              \n");
	gotoxy(50,31);printf("                            H E L P                                  \n");
	gotoxy(50,32);printf("                            A B O U T  U S                           \n");
	gotoxy(50,33);printf("                            E X I T                                  \n");	
Sleep(150);
menu=cursormenu(4,73,30);switch(menu)
	{
		case 1: player();break;
		case 2: rule();break;
		case 3: aboutus();break;
		case 4: exit();
	}	
}
// Modul help
void rule()
{
	setcolor(13);
	system("cls");
	FILE *f_howtoplay;	
	char rule[panjang];
	
	f_howtoplay = fopen("howtoplay.txt","r");
	
	while ((fgets(rule,panjang,f_howtoplay)) != NULL)
	printf("%s\r",rule);
	
	fclose(f_howtoplay);
	getch();
system("CLS");
	menu_utama();	
}
// Modul about us
void aboutus()
{
	system("cls");
	FILE *f_aboutus;
 	char string[panjang];
 	char namafile[65];
	
	//buka file untuk dibaca isinya
   	if((f_aboutus=fopen("aboutus.TXT", "rt")) == NULL)
   	{
		printf("File tidak dapat dibuka!\r\n");
		exit(1);
	}
	printf("\n");
    //ambil isi file menggunakan fungsi fread(), lalu tampilkan ke layar
    while ((fgets(string, panjang, f_aboutus)) != NULL)
  	//tampilkan string ke layar
   	printf("%s\r", string); 
   	fclose(f_aboutus);
   	getch();
   	system("cls");
   	menu_utama();
}
// Modul Exit
void exit()
{
	system("cls");
	printf("\n\n\n\n\t\t    ");
	char a[]={"Thank's for playing this game :D..\n"}; 
	int i; 
	double j; 
	for(i=0;a[i]!='.';i++) 
	{ 
		printf("%c",a[i]); 
	} 
	
	printf("\n\n\n\n\n\n\n");
	printf("\t���������������������������������������������������������ͻ\n");
	printf("\t�                                                         �\n");
	printf("\t�  copyright 2016 || Proyek 2 D3 - Teknik Informatika     �\n");
	printf("\t�                                                         �\n");
	printf("\t���������������������������������������������������������ͼ\n");
	printf("\n\n\n\n\n");
 exit(1);
}
// Modul memilih jumlah pemain
void player()
{
	system("cls");
	char menu;
	setcolor(15);
	gotoxy(40,15);printf("������������������������������������������������������������������������������������  \n");
	gotoxy(40,16);printf("��                                                                                ��  \n");
	gotoxy(40,17);printf("��  ���������   ���           ��������   ���      ���  ����������  ��������       ��  \n");
	gotoxy(40,18);printf("��  ����������  ���          ����������   ���    ���   ����������  ���������      ��  \n");
	gotoxy(40,19);printf("��  ���    ���  ���          ���    ���    ���  ���    ���         ���    ���     ��  \n");
	gotoxy(40,20);printf("��  ���    ���  ���          ���    ���     ������     ���         ���    ���     ��  \n");
	gotoxy(40,21);printf("��  ����������  ���          ����������      ����      ����������  ���������      ��  \n");
	gotoxy(40,22);printf("��  ���������   ���          ����������       ��       ����������  ��������       ��  \n");
	gotoxy(40,23);printf("��  ���         ���          ���    ���       ��       ���         ���   ���      ��  \n");
	gotoxy(40,24);printf("��  ���         ���          ���    ���       ��       ���         ���    ���     ��  \n");
	gotoxy(40,25);printf("��  ���         �����������  ���    ���       ��       ����������  ���     ���    ��  \n");
	gotoxy(40,26);printf("��  ���         �����������  ���    ���       ��       ����������  ���      ���   ��  \n");
	gotoxy(40,27);printf("��                                                                                ��  \n");
	gotoxy(40,28);printf("������������������������������������������������������������������������������������  \n");
	gotoxy(40,29);printf("\n");
	gotoxy(40,30);printf("                                O N E P L A Y E R          \n");
	gotoxy(40,31);printf("                                M U L T I P L A Y E R        \n");
	gotoxy(40,32);printf("                                M E N U                \n");
	gotoxy(40,33);printf("                                E X I T                \n");
menu=cursormenu(4,70,30);
switch(menu)
	{
		case 1: 
				oneplayer();break;
		case 2: 
				multiplayer();break;
		case 3: 
				menu_utama();break;
		case 4: exit();
	}
}
// Modul Memilih one player
void oneplayer()
{
system("cls");
char menu;
setcolor(15);
	gotoxy(50,18);printf("������������������������������������������������������������  \n");
	gotoxy(50,19);printf("��                                                        ��  \n");
	gotoxy(50,20);printf("��     ������      ���                                    ��  \n");
	gotoxy(50,21);printf("��   ��������      ���                                    ��  \n");
	gotoxy(50,22);printf("��       ����      ���                                    ��  \n");
	gotoxy(50,23);printf("��       ����      ���          E A S Y                   ��  \n");
	gotoxy(50,24);printf("��       ����      ���          M E D I U M               ��  \n");
	gotoxy(50,25);printf("��       ����      ���          H A R D                   ��  \n");
	gotoxy(50,26);printf("��       ����      ���          M E N U                   ��  \n");
	gotoxy(50,27);printf("��       ����      ���                                    ��  \n");
	gotoxy(50,28);printf("��  �������������  ���                                    ��  \n");
	gotoxy(50,29);printf("��  �������������  ���                                    ��  \n");
	gotoxy(50,30);printf("��                                                        ��  \n");
	gotoxy(50,31);printf("������������������������������������������������������������	\n");
Sleep(150);
menu=cursormenu(4,80,23);
	switch(menu)
	{
		case 1: 
				match(1, 0); break;
		case 2: 
				match(1,0); break;
		case 3: 
				match(1, 0); break; 
		case 4:
				menu_utama();
	}
} 
// Modul Memilih two player
void multiplayer()
{
system("cls");
char menu;
setcolor(15);
	gotoxy(50,18);printf("������������������������������������������������������������  \n");
	gotoxy(50,19);printf("��                                                        ��  \n");
	gotoxy(50,20);printf("��   �����������      ���                                 ��  \n");
	gotoxy(50,21);printf("��   ������������     ���                                 ��  \n");
	gotoxy(50,22);printf("��            ���     ���                                 ��  \n");
	gotoxy(50,23);printf("��          ����      ���       O N E VS O N E            ��  \n");
	gotoxy(50,24);printf("��         ����       ���       T W O VS T W O            ��  \n");
	gotoxy(50,25);printf("��        ����        ���       R O Y A L R U M B LE      ��  \n");
	gotoxy(50,26);printf("��      ����          ���       M E N U                   ��  \n");
	gotoxy(50,27);printf("��    �����           ���                                 ��  \n");
	gotoxy(50,28);printf("��   �������������    ���                                 ��  \n");
	gotoxy(50,29);printf("��    ������������    ���                                 ��  \n");
	gotoxy(50,30);printf("��                                                        ��  \n");
	gotoxy(50,31);printf("������������������������������������������������������������	\n");
Sleep(150);
menu=cursormenu(4,80,23);
	switch(menu)
	{
		case 1: 
				zplayer = 1; match(2, 0);  break;
		case 2: 
				zplayer = 2; match(4, 0);  break;
		case 3: 
				zplayer = 3; match(4, 0);  break;
		case 4:
				menu_utama();
	} 
} 
int main()
{
	fillSmallBoard();
	//MaximizeWindows();
	tampilan_awal();
	menu_utama();
  return 0;
}
void First_Cursor_Position(){
		gotoxy(14,15);
}
void x_symbol(int x,int y)
{
	int a;
	x = x - 3;
	y = y - 4;
//	a = rand() % 8;
//	SetColor(a);
	SetColor(10);
	gotoxy(x,y+0);printf("%c%c    %c%c",219,219,219,219);
	gotoxy(x,y+1);printf(" %c%c  %c%c ",219,219,219,219);
	gotoxy(x,y+2);printf("  %c%c%c%c  ",219,219,219,219);
	gotoxy(x,y+3);printf(" %c%c  %c%c ",219,219,219,219);
	gotoxy(x,y+4);printf("%c%c    %c%c",219,219,219,219);
}
void o_symbol(int x,int y)
{
	x = x - 3;
	y = y - 4;	
	SetColor(12);	
	gotoxy(x,y+0);printf(" %c%c%c%c%c%c ",219,219,219,219,219,219);
	gotoxy(x,y+1);printf("%c%c    %c%c",219,219,219,219);
	gotoxy(x,y+2);printf("%c%c    %c%c",219,219,219,219);
	gotoxy(x,y+3);printf("%c%c    %c%c",219,219,219,219);
	gotoxy(x,y+4);printf(" %c%c%c%c%c%c ",219,219,219,219,219,219);
}
void x_symbol2(int x,int y)
{
	int a;
	x = x - 3;
	y = y - 4;
//	a = rand() % 8;
//	SetColor(a);
	SetColor(15);
	gotoxy(x,y+0);printf("%c%c    %c%c",219,219,219,219);
	gotoxy(x,y+1);printf(" %c%c  %c%c ",219,219,219,219);
	gotoxy(x,y+2);printf("  %c%c%c%c  ",219,219,219,219);
	gotoxy(x,y+3);printf(" %c%c  %c%c ",219,219,219,219);
	gotoxy(x,y+4);printf("%c%c    %c%c",219,219,219,219);
}
void o_symbol2(int x,int y)
{
	x = x - 3;
	y = y - 4;	
	SetColor(17);	
	gotoxy(x,y+0);printf(" %c%c%c%c%c%c ",219,219,219,219,219,219);
	gotoxy(x,y+1);printf("%c%c    %c%c",219,219,219,219);
	gotoxy(x,y+2);printf("%c%c    %c%c",219,219,219,219);
	gotoxy(x,y+3);printf("%c%c    %c%c",219,219,219,219);
	gotoxy(x,y+4);printf(" %c%c%c%c%c%c ",219,219,219,219,219,219);
}

void BoardGame()
{
	BoardInfo();
	//board(30,45);
}

void BoardInfo()
{
    system("cls");
	int x,y,i,j;

    //Box Pertama
    gotoxy(x=66,y=4); printf("����������������������������������������������������������������������������������������������ͻ");
    for (i=0;i<3;i++)
    {
    	gotoxy(x=66,y=5+i); printf("�                                                                                              �");
    	gotoxy(x=66,y=6+i); printf("�                                                                                              �");
    	gotoxy(x=66,y=7+i); printf("�                                                                                              �");
    }
    gotoxy(x=66,y=7+i); printf("����������������������������������������������������������������������������������������������ͼ");
	SetColor(15);
    //Box kedua
    gotoxy(x=66,y=9+i); printf("����������������������������������������������������������������������������������������������ͻ");
    for (j=0;j<(43-(9+i));j++)
    {
        gotoxy(x=66,y=(10+i)+j); printf("�                                                                                              �");
        gotoxy(x=66,y=(11+i)+j); printf("�                                                                                              �");
    }
    gotoxy(x=66,y=((11+i)+j)); printf("����������������������������������������������������������������������������������������������ͼ");
    gotoxy(x=71,y=15); printf("USE YOUR NAVIGATION BUTTON TO SELECT THE BOX THAT YOU WANT:");
    gotoxy(x=71,y=17); printf(" %c : UP",24);
    gotoxy(x=71,y=18);printf(" %c : DOWNN",25);
    gotoxy(x=71,y=19);printf(" %c : LEFT",27);
    gotoxy(x=71,y=20);printf(" %c : RIGHT",26);
    gotoxy(x=71,y=21);printf(" PRESS ENTER TO DO YOUR TURN",26);
   	First_Cursor_Position();
}
int KocokDadu()
{
	//random dadu
	int i,dadu;	
	srand(time(NULL));
		gotoxy(70,32);printf("%c%c%c%c%c%c%c%c%c%c%c\n",218,196,196,196,196,196,196,196,196,196,191);
		gotoxy(70,33);printf("%c         %c\n",179,179);
		gotoxy(70,34);printf("%c         %c\n",179,179);
		gotoxy(70,35);printf("%c         %c\n",179,179);
		gotoxy(70,36);printf("%c         %c\n",179,179);
		gotoxy(70,37);printf("%c         %c\n",179,179);
		gotoxy(70,38);printf("%c%c%c%c%c%c%c%c%c%c%c\n",192,196,196,196,196,196,196,196,196,196,217);
		for(i=0;i<20;i++)
		{	
	dadu = (rand()% (6)+1);	
	switch(dadu) //untuk menampilkan tampilan kocokan mata dadu 
		{
		case 1:
			gotoxy(71,33);printf("        ");
			gotoxy(71,34);printf("        ");
			gotoxy(71,35);printf("    o   ");
			gotoxy(71,36);printf("        ");
			gotoxy(71,37);printf("        ");
			break;
		case 2:
			gotoxy(71,33);printf("       o ");
			gotoxy(71,34);printf("         ");
			gotoxy(71,35);printf("         ");
			gotoxy(71,36);printf("         ");
			gotoxy(71,37);printf(" o       ");
			
			break;
		case 3:
			gotoxy(71,33);printf("       o ");
			gotoxy(71,34);printf("         ");
			gotoxy(71,35);printf("    o    ");
			gotoxy(71,36);printf("         ");
			gotoxy(71,37);printf(" o       ");
			break;
		case 4:
			gotoxy(71,33);printf(" o     o ");
			gotoxy(71,34);printf("         ");
			gotoxy(71,35);printf("         ");
			gotoxy(71,36);printf("         ");
			gotoxy(71,37);printf(" o     o ");
			break;
		case 5:
			gotoxy(71,33);printf(" o     o ");
			gotoxy(71,34);printf("         ");
			gotoxy(71,35);printf("    o    ");
			gotoxy(71,36);printf("         ");
			gotoxy(71,37);printf(" o     o ");
			break;
		case 6:
			gotoxy(71,33);printf(" o     o ");
			gotoxy(71,34);printf("         ");
			gotoxy(71,35);printf(" o     o ");
			gotoxy(71,36);printf("         ");
			gotoxy(71,37);printf(" o     o ");
			break;
		}

		Sleep(50);	
		}
		return dadu;
}

int GiliranKocok(int jumlahplayer)
{
int A[2], DapatDadu, i=1;
int Hasil;
	while(i<=2){
		gotoxy(71,30);printf("Press Any Key To Roll The Dice");
		if(jumlahplayer <= 2){
			gotoxy(71,28); PrintPlayer(pemain, i); printf("Turn");
		}else{
			gotoxy(71,28);printf("Team %d Turn", i);
		}
		getch();
		A[i]= KocokDadu();
		if(jumlahplayer <= 2){
			gotoxy(82,32+i);PrintPlayer(pemain, i); printf("%d", A[i]);
		}else{
			gotoxy(82+i,32+i);printf("Team %d : %d", i, A[i]);
		}
	 	i++;
	}
	if (A[1] == A[2]){
		 	getch();
		 	GiliranKocok(jumlahplayer);
		 }
	if(A[1] >= A[2]){Hasil = 1;}else{Hasil = 2;}
	if(jumlahplayer <= 2){
		gotoxy(82,36);PrintPlayer(pemain, Hasil);printf("Got First Turn", Hasil);
	}else{
		gotoxy(82,36);printf("Team %d Got First Turn", Hasil);
	}
	getch();
	int j = 28;
	while(j <= 41){gotoxy(68,j);printf("                            		    	  \n"); j++;}
	return Hasil;
}
void winner(){

	gotoxy(48, 18);printf("     ***     ***  ***    ***    ***        ***     *******   ******* ");
	gotoxy(48, 19);printf("      ***   *** **   **  ***    ***       *****    ********  ******* ");
	gotoxy(48, 20);printf("       *** *** **     ** ***    ***      *** ***   ***   *** ***      ");
	gotoxy(48, 21);printf("        *****  **     ** ***    ***     ***   ***  ***   *** ******* ");
	gotoxy(48, 22);printf("         ***   **     ** ***    ***    *********** *******   ***       ");
	gotoxy(48, 23);printf("         ***    **   **   ***  ***     ***     *** ***   *** ******* ");
	gotoxy(48, 24);printf("         ***      ***      ******      ***     *** ***   *** *******  ");
			
	gotoxy(39, 27);	printf(" ********** ***    *** *******     ***      ***  ***  ***   *** ***   *** ******* *******      *   * ");
	gotoxy(39, 28);	printf(" ********** ***    *** ***         ***      ***  ***  ****  *** ****  *** ***     *********   *** ***");
	gotoxy(39, 29);	printf("     ***    ***    *** ***         ***      ***  ***  ***** *** ***** *** ***     **    ***   *** ***");
	gotoxy(39, 30);	printf("     ***    ********** *******     *** **** ***  ***  ********* ********* ******* **    ***   *** ***");
	gotoxy(39, 31);	printf("     ***    ********** ***         *** **** ***  ***  *** ***** *** ***** ***     *******            ");
	gotoxy(39, 32);	printf("     ***    ***    *** ***         ****    ****  ***  ***  **** ***  **** ***     **    **    *** ***");
	gotoxy(39, 33);	printf("     ***    ***    *** *******     ****    ****  ***  ***   *** ***  **** ******* **     **   *** ***");
}

