#include"tictactoe.h"
#include<malloc.h>

void CreateList(List *L){
	Head(*L) = Empty;
}
void EntryPlayer(List *L, info Player ){
	address P;
	if(Head(*L) == NULL){
		Head(*L) = AllocationPlayer(Player);	
	}else{
		P = Head(*L);
		if(P->next != Empty){
			while(P->next != Head(*L)){
				P =  P->next;
			}			
		}
		P->next = AllocationPlayer(Player);
		P = P->next;
		P->next = Head(*L);
	}	
}
void EntryBot(List *L){
	address P;
	info Komputer;
	strcpy(Komputer, "Computer");
	if(Head(*L) == NULL){
		Head(*L) = AllocationBot(Komputer);	
	}else{
		P = Head(*L);
		if(P->next != Empty){
			while(P->next != Head(*L)){
				P =  P->next;
			}			
		}
		P->next = AllocationBot(Komputer);
		P = P->next;
		P->next = Head(*L);
	}	
}
int CountPlayer(List L){
	int i = 1;
	address P;
	if(Head(L) != Empty){
		P = Head(L);
		for(;;){
			if(Next(P) != Head(L)){
				P = Next(P);
				i++;
			}else{
				return i;
			} 
		}
	}
}

address AllocationPlayer(info Data){
	address P;
	P = (address) malloc (sizeof (players));
	if(P != Empty){
		strcpy(Player(P), Data);
			IsBot(P) = false;
		
		Mark(P) = Empty;
		Next(P) = Empty;
		return(P);
	}else{
		printf("Allocation Failed");
	} 
}
address AllocationBot(info Data){
	address P;
	P = (address) malloc (sizeof (players));
	if(P != Empty){
		strcpy(Player(P), Data);
			IsBot(P) = true;
		
		Mark(P) = Empty;
		Next(P) = Empty;
		return(P);
	}else{
		printf("Allocation Failed");
	} 
}
bool CheckPlayerIsBot(List L, int position){
	int a = 1;
	address P;
	if(Head(L) != Empty ){
		P = Head(L);
		for(;;){
			if(a == position){
				break;
			}
			P = Next(P);
			a++;
		}
		return IsBot(P);
	}else{
		printf("There is no player yet");
	}
}
void PrintAllPlayers(List L){
	int i = 1;
	address P;
	if(Head(L) != Empty){
		P = Head(L);
		for(;;){
			gotoxy(72,4+i); 
			if(Mark(P) == Empty){
				printf("-_-");
			}
			printf("Player %d : %s -> %c\t",i, Player(P), Mark(P));
			if(Next(P) != Head(L)){
				P = Next(P);
				i++;
			}else{
				break;
			} 
		}
	}else{
		printf("There is no player yet");
	}
}
void PrintPlayerNameTurn(address Position){
	printf("%s ", Position->player); 
}
char GetPlayerMark(address Position){
	return Position->mark;
}
void PointToNextTurn(address *Position){
	address P;
	P = *Position;
	*Position = Next(P);
}

address PointFirstTurn(List L, int position){
	address P;
	int i = 1;
	if(Head(L) != Empty ){
		P = Head(L);
		for(;;){
			if(i == position){
			return P;
			}
			P = Next(P);
			i++;
		}
	}else{
		printf("There is no player yet");
	}
}
void PrintPlayer(List L, int position){
	int a = 1;
	address P;
	if(Head(L) != Empty ){
		P = Head(L);
		for(;;){
			if(a == position){
				break;
			}
			P = Next(P);
			a++;
		}
		printf("%s ", Player(P));
	}else{
		printf("There is no player yet");
	}
}
void SetPlayerMark(List *L, int position, char mark){
	int i = 1;
	address P;
	if(Head(*L) != Empty){
		P = Head(*L);
		if(position == 1){
			if(mark == 'x'){
				Mark(P) = 'X';
				P = Next(P);
				Mark(P) = 'O';
				P = Next(P);
				Mark(P) = 'X';
				P = Next(P);
				Mark(P) = 'O';				
			}else if(mark == 'o'){
				Mark(P) = 'O';
				P = Next(P);
				Mark(P) = 'X';
				P = Next(P);
				Mark(P) = 'O';
				P = Next(P);
				Mark(P) = 'X';
			}
		}else if(position == 2){
			if(mark == 'x'){
				Mark(P) = 'O';
				P = Next(P);
				Mark(P) = 'X';
				P = Next(P);
				Mark(P) = 'O';
				P = Next(P);
				Mark(P) = 'X';
			}else if(mark == 'o'){
				Mark(P) = 'X';				
				P = Next(P);
				Mark(P) = 'O';
				P = Next(P);
				Mark(P) = 'X';
				P = Next(P);
				Mark(P) = 'O';
			}
		}
				
	}
}
void CreateBoard(int size)
{
	int row, column;
	row = size * 6;
	column = size * 9;
	int papan[row+1][column+1];		   			//Row dan Column di lebihkan 1 agar tidak terjadi error
	int i,j;
	for(i=0;i<=row;i++)
	{
		for(j=0;j<=column;j++)
		{
			if(i%6==0)
			{
				if(j%9==0)
				{
					if(j==0 && i==0)
					{
						papan[i][j] = 201;		//board simpang 2 kiri atas
					}
					if(i==0 && j!=0)			
					{
						papan[i][j] = 203;		//board simpang 3 bagian atas
					}
					if(j==column)
					{
						papan[i][j] = 187;		//board simpang 2 kanan atas
					}
					if(i!=0 && j==0)
					{
						papan[i][j] = 199;		//board simpang 3 bagian kiri
					}
					if(i!=0 && j==column)
					{
						papan[i][j] = 185;		//board simpang 3 bagian kanan
					}
					if(i==row && j==0)
					{
						papan[i][j] = 200;		//board simpang 2 kiri bawah
					}
					if(i==row && j==column)
					{
						papan[i][j]= 188;		//board simpan 2 kanan bawah
					}
					if(i>0 && i<row && j>0 && j <column)
					{
						papan[i][j] = 206;		//board simpang 4 area tengah
					}
					if(i==row && j!=0 && j!=column)
					{
						papan[i][j] = 202;		//board simpang 3 bagian bawah
					}
				}
				else
				{
					papan[i][j] = 205; 			//board border baris
				}
			}
			else
			{
				if(j%9==0)
				{
					papan[i][j] = 186;			//board border kolom
				}
				else
				{
					papan[i][j] = ' ';			//board mengisi kosong
				}
			}
		}
		printf("\n");
	}
	
	for(i=0;i<=row;i++)
	{
		for(j=0;j<=column;j++)
		{
			SetColor(15);
			gotoxy(j+10,i+10);printf("%c",papan[i][j]);
		}
		printf("\n");
	}
}

void setcolor(int warna) //modul yang berfungsi untuk memberi warna ke karakter
{ 
	HANDLE hConsole; 
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, warna);
}
void MaximizeWindows(void){
	HWND consoleWindow = GetConsoleWindow();
	ShowWindow(consoleWindow, SW_MAXIMIZE);
}
void gotoxy(int x, int y)
{
	HANDLE hConsoleOutput;  
	COORD dwCursorPosition;  
	dwCursorPosition.X = x;  
	dwCursorPosition.Y = y;  
	hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);  
	SetConsoleCursorPosition(hConsoleOutput,dwCursorPosition);   
}
void SetColor(int ForgC)
{
     WORD wColor;
     //This handle is needed to get the current background attribute
     HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
     CONSOLE_SCREEN_BUFFER_INFO csbi;
     //csbi is used for wAttributes word
     if(GetConsoleScreenBufferInfo(hStdOut, &csbi))
     {
          //To mask out all but the background attribute, and to add the color
          wColor = (csbi.wAttributes & 0xF0) + (ForgC & 0x0F);
          SetConsoleTextAttribute(hStdOut, wColor);   
     }
     return;
}


